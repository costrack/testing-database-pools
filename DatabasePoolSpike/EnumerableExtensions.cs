﻿using System;
using System.Collections.Generic;

namespace DatabasePoolSpike
{
	public static class EnumerableExtensions
	{
		public static HashSet<T> ToHashSet<T>(this IEnumerable<T> values, IEqualityComparer<T> comparer = null)
		{
			return new HashSet<T>(values, comparer);
		}
	}
}