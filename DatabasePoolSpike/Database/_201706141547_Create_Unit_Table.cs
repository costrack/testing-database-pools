﻿using System;

using FluentMigrator;

namespace DatabasePoolSpike.Database
{
	[Migration(201706141547)]
	public class _201706141547_Create_Unit_Table
		: AutoReversingMigration
	{
		public override void Up()
		{
			Create.Table("Unit")
				.WithColumn("Id").AsGuid().PrimaryKey()
				.WithColumn("AreaId").AsGuid().ForeignKey("FK_Unit_Area", "Area", "Id")
				.WithColumn("Name").AsString(20).NotNullable();
		}
	}
}