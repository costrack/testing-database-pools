﻿using System;

namespace DatabasePoolSpike.Database
{
	public interface IMigrator
	{
		event EventHandler<MigrateEventArgs> Migrating;
		event EventHandler<MigrateEventArgs> Migrated;
	}
}