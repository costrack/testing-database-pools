﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;

using FluentMigrator;
using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Generators.SqlServer;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;

namespace DatabasePoolSpike.Database
{
	public class Migrator
		: IMigrator
	{
		public void Migrate(SqlConnection cnn)
		{
			var announcer = new TextWriterAnnouncer(s => Trace.WriteLine(s));
			var runnerContext = new RunnerContext(announcer);
			var generator = new SqlServer2014Generator();
			var options = new ProcessorOptions();
			var factory = new SqlServerDbFactory();
			var processor = new SqlServerProcessor(cnn, generator, announcer, options, factory);
			var runner = new MigrationRunner(typeof(Migrator).Assembly, runnerContext, processor);

			SortedList<long, IMigrationInfo> sortedList = runner.MigrationLoader.LoadMigrations();
			var scopeHandler = new MigrationScopeHandler(runner.Processor);
			using (IMigrationScope wrapMigrationScope = scopeHandler.CreateOrWrapMigrationScope(runner.TransactionPerSession))
			{
				try
				{
					runner.ApplyMaintenance(MigrationStage.BeforeAll, true);
					foreach (KeyValuePair<long, IMigrationInfo> keyValuePair in sortedList)
					{
						var migration = keyValuePair.Value;
						var ea = new MigrateEventArgs(runner, migration);

						runner.ApplyMaintenance(MigrationStage.BeforeEach, true);
						Migrating(this, ea);

						runner.ApplyMigrationUp(migration, migration.TransactionBehavior == TransactionBehavior.Default);
						Migrated(this, ea);
						runner.ApplyMaintenance(MigrationStage.AfterEach, true);
					}
					runner.ApplyMaintenance(MigrationStage.BeforeProfiles, true);
					runner.ApplyProfiles();
					runner.ApplyMaintenance(MigrationStage.AfterAll, true);
					wrapMigrationScope.Complete();
				}
				catch
				{
					if (wrapMigrationScope.IsActive)
					{
						wrapMigrationScope.Cancel();
					}
					throw;
				}
			}
			runner.VersionLoader.LoadVersionInfo();
		}

		public event EventHandler<MigrateEventArgs> Migrating = delegate { };
		public event EventHandler<MigrateEventArgs> Migrated = delegate { };
	}
}