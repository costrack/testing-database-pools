﻿using System;

using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;

namespace DatabasePoolSpike.Database
{
	public class MigrateEventArgs
		: EventArgs
	{
		public MigrateEventArgs(MigrationRunner runner, IMigrationInfo migrationInfo)
		{
			Runner = runner;
			MigrationInfo = migrationInfo;
		}

		public MigrationRunner Runner { get; }
		public IMigrationInfo MigrationInfo { get; }
	}
}