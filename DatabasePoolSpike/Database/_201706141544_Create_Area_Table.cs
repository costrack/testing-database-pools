﻿using System;

using FluentMigrator;

namespace DatabasePoolSpike.Database
{
	[Migration(201706141544)]
	public class _201706141544_Create_Area_Table
		: AutoReversingMigration
	{
		public override void Up()
		{
			Create.Table("Area")
				.WithColumn("Id").AsGuid().PrimaryKey()
				.WithColumn("Name").AsString(20).NotNullable();
		}
	}
}