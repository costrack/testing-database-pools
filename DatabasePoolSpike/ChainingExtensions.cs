﻿using System;
using System.Collections.Generic;

namespace DatabasePoolSpike
{
	public static class ChainingExtensions
	{
		public static IEnumerable<T> Chain<T>(this T item, Func<T, T> getNext)
		{
			var next = item;
			while (next != null)
			{
				yield return next;
				next = getNext(next);
			}
		}
	}
}