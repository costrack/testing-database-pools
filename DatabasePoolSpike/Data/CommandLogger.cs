using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;

using log4net;

namespace DatabasePoolSpike.Data
{
	public static class CommandLogger
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(CommandLogger));

		public static TCommand Logged<TCommand>(this TCommand command, bool includeParameters = true)
			where TCommand : IDbCommand
		{
			return command.Logged(Log, includeParameters);
		}

		public static TCommand Logged<TCommand>(this TCommand command, ILog log, bool includeParameters = true)
			where TCommand : IDbCommand
		{
			log ??= Log;
			log.LogCommand(command, includeParameters);
			return command;
		}

		public static void LogCommand(this ILog log, IDbCommand command, bool includeParameters = true)
		{
			log ??= Log;
			bool enabled = log.IsDebugEnabled || Debugger.IsAttached;

			if (enabled && !string.IsNullOrEmpty(command.CommandText))
			{
				if (includeParameters)
				{
					string text = command.GetCommandTextWithParameters();
					log.DebugFormat("command: {0}", text);
				}
				else
				{
					log.DebugFormat("command: {0}", command.CommandText);
				}
			}
		}

		private static readonly Dictionary<DbType, string> DbTypeToSqlVariableType = new Dictionary<DbType, string>()
		{
			{ DbType.Int32, "int" },
			{ DbType.Int16, "short" },
			{ DbType.Int64, "bigint" },
			{ DbType.Single, "float" },
			{ DbType.Double, "float" },
		};

		public static string GetCommandTextWithParameters(this IDbCommand command)
		{
			var sb = new StringBuilder();
			for (int index = 0; index < command.Parameters.Count; index++)
			{
				var param = (IDbDataParameter)command.Parameters[index];
				if (index > 0)
				{
					sb.AppendLine();
				}
				var parameterName = param.ParameterName;
				if (!parameterName.StartsWith("@"))
				{
					parameterName = "@" + parameterName;
				}
				sb.Append("declare ").Append(parameterName).Append(" ");

				string sqlType;
				string sqlValue = null;
				if (param.Value == null || param.Value is DBNull)
				{
					sqlValue = "null";
				}
				switch (param.DbType)
				{
					case DbType.Boolean:
						sqlType = "bit";
						if (param.Value is bool value)
						{
							sqlValue = value ? "1" : "0";
						}
						else
						{
							sqlValue = sqlValue ?? param.Value.ToString();
						}
						break;

					case DbType.Decimal:
						if (param.Size != 0)
						{
							sqlType = $"decimal({param.Size}, {param.Precision})";
						}
						else
						{
							sqlType = "decimal";
						}
						sqlValue = sqlValue ?? param.Value.ToString();
						break;

					case DbType.AnsiString:
					case DbType.String:
						sqlType = $"nvarchar({param.Size})";
						sqlValue = sqlValue ?? "'" + param.Value.ToString().Replace("'", "''") + "'";
						break;

					case DbType.Date:
					case DbType.DateTime:
					case DbType.DateTime2:
						sqlType = "datetime";
						if (param.Value is DateTime date)
						{
							sqlValue = sqlValue ?? $"'{date:yyyy-MM-dd HH:mm:ss}'";
						}
						else
						{
							sqlValue = sqlValue ?? param.Value.ToString();
						}
						break;

					default:
						if (!DbTypeToSqlVariableType.TryGetValue(param.DbType, out sqlType))
						{
							sqlType = param.DbType.ToString();
						}

						sqlValue = sqlValue ?? param.Value.ToString();
						break;
				}
				sb.Append(sqlType).Append(" = ").Append(sqlValue);
			}

			if (command.Parameters.Count > 0)
			{
				sb.AppendLine().AppendLine();
			}
			sb.Append(command.CommandText);
			return sb.ToString();
		}
	}
}