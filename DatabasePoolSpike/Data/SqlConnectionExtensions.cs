﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace DatabasePoolSpike.Data
{
	public static class SqlConnectionExtensions
	{
		public static SqlConnectionStringBuilder Clone(this SqlConnectionStringBuilder csb)
		{
			return new SqlConnectionStringBuilder(csb.ConnectionString);
		}

		[SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Global")]
		public class SysDatabaseInformation
		{
			public string Name { get; set; }
			public int DatabaseId { get; set; }
			public int? SourceDatabaseId { get; set; }
			public DateTime CreateDate { get; set; }
			public bool IsReadOnly { get; set; }
		}

		public static void TraceInfoMessages(this SqlConnection cnn, log4net.ILog log)
		{
			SqlInfoMessageEventHandler handleInfoMessage = (s, ea) =>
			{
				log.DebugFormat("SQL info message: {0} - {1}", ea.Source, ea.Message);
				foreach (SqlError error in ea.Errors)
				{
					log.ErrorFormat("SQL error: {0}", error.Message);
				}
			};
			cnn.InfoMessage += handleInfoMessage;

			EventHandler handleDisposed = null;
			handleDisposed = (s, ea) =>
			{
				cnn.Disposed -= handleDisposed;
				cnn.InfoMessage -= handleInfoMessage;
			};
			cnn.Disposed += handleDisposed;
		}

		public static bool DatabaseExists(this SqlConnection cnn, string dbName, SqlTransaction tran = null)
		{
			return DatabaseExists(cnn, dbName, tran, out _);
		}

		public static bool DatabaseExists(this SqlConnection cnn, string dbName, SqlTransaction tran, out SysDatabaseInformation details)
		{
			using (var command = cnn.CreateCommand())
			{
				if (tran != null)
				{
					command.Transaction = tran;
				}
				command.CommandText = "select name, database_id, source_database_id, create_date, is_read_only from sys.databases where name = @dbName";
				command.Parameters.AddWithValue("dbName", dbName);

				using (var dr = command.Logged().ExecuteReader())
				{
					if (!dr.Read())
					{
						details = null;
						return false;
					}

					int index = -1;
					details = new SysDatabaseInformation()
					{
						Name = dr.GetString(++index),
						DatabaseId = dr.GetInt32(++index),
						SourceDatabaseId = dr.IsDBNull(++index) ? (int?)null : dr.GetInt32(index),
						CreateDate = dr.GetDateTime(++index),
						IsReadOnly = dr.GetBoolean(++index),
					};
					return true;
				}
			}
		}

		public static object ExecuteScalar(this SqlConnection cnn, string query, SqlTransaction tran = null, log4net.ILog log = null, Dictionary<string, object> parameters = null)
		{
			using (var command = cnn.CreateCommand())
			{
				if (tran != null)
				{
					command.Transaction = tran;
				}
				command.CommandText = query;

				if (parameters != null)
				{
					foreach (var pair in parameters)
					{
						command.Parameters.AddWithValue(pair.Key, pair.Value);
					}
				}

				object result = command.Logged(log).ExecuteScalar();
				return result;
			}
		}

		public static int ExecuteNonQuery(this SqlConnection cnn, string query, SqlTransaction tran = null, log4net.ILog log = null)
		{
			using (var command = cnn.CreateCommand())
			{
				if (tran != null)
				{
					command.Transaction = tran;
				}
				command.CommandText = query;
				int result = command.Logged(log).ExecuteNonQuery();
				return result;
			}
		}

		public static void KillConnectionsTo(this SqlConnection cnn, string database, SqlTransaction tran = null)
		{
			using (var command = cnn.CreateCommand())
			{
				var kill = new HashSet<int>();

				if (tran != null)
				{
					command.Transaction = tran;
				}
				command.CommandText = "select session_id from sys.dm_exec_sessions where database_id = db_id(@db)";
				command.Parameters.AddWithValue("db", database);

				using (var dr = command.Logged().ExecuteReader())
				{
					while (dr.Read())
					{
						int id = dr.GetInt16(0);
						kill.Add(id);
					}
				}

				foreach (var id in kill)
				{
					command.CommandText = $"kill {id}";
					command.Logged().ExecuteNonQuery();
				}
			}
		}

		public static SqlConnection OpenConnectionToCreatedDb(SqlConnectionStringBuilder csb, string dbName, out SysDatabaseInformation info)
		{
			csb = csb.Clone();
			csb.InitialCatalog = "master";

			var cnn = new SqlConnection(csb.ConnectionString);
			try
			{
				cnn.Open();

				if (!cnn.DatabaseExists(dbName, null, out info))
				{
					cnn.ExecuteNonQuery($"create database [{dbName}]");
					cnn.DatabaseExists(dbName, null, out info);
					System.Diagnostics.Debug.Assert(info != null, "Database was not created");
				}
				cnn.ChangeDatabase(dbName);
			}
			catch (Exception)
			{
				cnn.Dispose();
				throw;
			}

			return cnn;
		}
	}
}