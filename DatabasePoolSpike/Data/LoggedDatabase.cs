﻿using System;
using System.Data;
using System.Data.Common;

using NPoco;

namespace DatabasePoolSpike.Data
{
	public class LoggedDatabase
		: NPoco.Database
	{
		public LoggedDatabase(DbConnection connection)
			: base(connection)
		{
		}

		public LoggedDatabase(DbConnection connection, DatabaseType dbType)
			: base(connection, dbType)
		{
		}

		public LoggedDatabase(DbConnection connection, DatabaseType dbType, IsolationLevel? isolationLevel)
			: base(connection, dbType, isolationLevel)
		{
		}

		public LoggedDatabase(DbConnection connection, DatabaseType dbType, IsolationLevel? isolationLevel, bool enableAutoSelect)
			: base(connection, dbType, isolationLevel, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionString, string providerName)
			: base(connectionString, providerName)
		{
		}

		public LoggedDatabase(string connectionString, string providerName, IsolationLevel isolationLevel)
			: base(connectionString, providerName, isolationLevel)
		{
		}

		public LoggedDatabase(string connectionString, string providerName, bool enableAutoSelect)
			: base(connectionString, providerName, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionString, string providerName, IsolationLevel? isolationLevel, bool enableAutoSelect)
			: base(connectionString, providerName, isolationLevel, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionString, DatabaseType dbType)
			: base(connectionString, dbType)
		{
		}

		public LoggedDatabase(string connectionString, DatabaseType dbType, IsolationLevel? isolationLevel)
			: base(connectionString, dbType, isolationLevel)
		{
		}

		public LoggedDatabase(string connectionString, DatabaseType dbType, IsolationLevel? isolationLevel, bool enableAutoSelect)
			: base(connectionString, dbType, isolationLevel, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionString, DatabaseType databaseType, DbProviderFactory provider)
			: base(connectionString, databaseType, provider)
		{
		}

		public LoggedDatabase(string connectionString, DatabaseType databaseType, DbProviderFactory provider, IsolationLevel? isolationLevel = null, bool enableAutoSelect = true)
			: base(connectionString, databaseType, provider, isolationLevel, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionStringName)
			: base(connectionStringName)
		{
		}

		public LoggedDatabase(string connectionStringName, IsolationLevel isolationLevel)
			: base(connectionStringName, isolationLevel)
		{
		}

		public LoggedDatabase(string connectionStringName, bool enableAutoSelect)
			: base(connectionStringName, enableAutoSelect)
		{
		}

		public LoggedDatabase(string connectionStringName, IsolationLevel? isolationLevel, bool enableAutoSelect)
			: base(connectionStringName, isolationLevel, enableAutoSelect)
		{
		}

		public log4net.ILog Log { get; set; }

		protected override void OnExecutingCommand(DbCommand cmd)
		{
			base.OnExecutingCommand(cmd.Logged(Log));
		}
	}
}