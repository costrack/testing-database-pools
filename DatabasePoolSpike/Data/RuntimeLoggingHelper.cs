using System;

using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;

namespace DatabasePoolSpike.Data
{
	public static class RuntimeLoggingHelper
	{
		public static Level GetLoggerLevel(Type type)
		{
			ILog log = LogManager.GetLogger(type);
			return GetLoggerLevel(log);
		}

		public static Level GetLoggerLevel(string loggerName)
		{
			ILog log = LogManager.GetLogger(loggerName);
			return GetLoggerLevel(log);
		}

		public static Level GetLoggerLevel(ILog logger)
		{
			Logger hierarchyLogger = (Logger)logger.Logger;
			return GetLoggerLevel(hierarchyLogger);
		}

		public static Level GetLoggerLevel(Logger hierarchyLogger)
		{
			if (hierarchyLogger.Level == null)
			{
				if (hierarchyLogger.Parent == null)
				{
					return null;
				}
				return GetLoggerLevel(hierarchyLogger.Parent);
			}
			return hierarchyLogger.Level;
		}

		public static void SetLoggerLevel(Type type, Level level)
		{
			ILog log = LogManager.GetLogger(type);
			SetLoggerLevel(log, level);
		}

		public static void SetLoggerLevel(string loggerName, Level level)
		{
			ILog log = LogManager.GetLogger(loggerName);
			SetLoggerLevel(log, level);
		}

		public static void SetLoggerLevel(ILog logger, Level level)
		{
			Logger hierarchyLogger = (Logger)logger.Logger;
			SetLoggerLevel(hierarchyLogger, level);
		}

		public static void SetLoggerLevel(Logger hierarchyLogger, Level level)
		{
			hierarchyLogger.Level = level;
		}
	}
}