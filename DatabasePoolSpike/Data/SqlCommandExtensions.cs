﻿using System;
using System.Data.SqlClient;
using System.Reflection;

namespace DatabasePoolSpike.Data
{
	public static class SqlCommandExtensions
	{
		private static readonly PropertyInfo ConnectionInfo = typeof(SqlConnection).GetProperty("InnerConnection", BindingFlags.NonPublic | BindingFlags.Instance);

		public static SqlTransaction GetTransaction(this SqlConnection cnn)
		{
			var internalConn = ConnectionInfo.GetValue(cnn, null);
			if (internalConn == null)
			{
				throw new ArgumentOutOfRangeException("cnn");
			}

			var currentTransactionProperty = internalConn.GetType().GetProperty("CurrentTransaction", BindingFlags.NonPublic | BindingFlags.Instance);
			System.Diagnostics.Debug.Assert(currentTransactionProperty != null, $"Could not find {ConnectionInfo}.CurrentTransaction property");
			var currentTransaction = currentTransactionProperty.GetValue(internalConn, null);
			if (currentTransaction == null)
			{
				return null;
			}

			var realTransactionProperty = currentTransaction.GetType().GetProperty("Parent", BindingFlags.NonPublic | BindingFlags.Instance);
			System.Diagnostics.Debug.Assert(realTransactionProperty != null, $"Could not find {currentTransactionProperty}.Parent property");
			var realTransaction = realTransactionProperty.GetValue(currentTransaction, null);
			return (SqlTransaction)realTransaction;
		}

		public static SqlCommand WithTransactionSet(this SqlCommand command)
		{
			if (command.Connection == null)
			{
				throw new InvalidOperationException("Command is not attached to a connection");
			}
			command.Transaction = command.Connection.GetTransaction();
			return command;
		}
	}
}