﻿using System;

using NPoco;

namespace DatabasePoolSpike.Core
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class Unit
	{
		public Guid Id { get; set; }
		public Guid? AreaId { get; set; }
		public string Name { get; set; }
	}
}