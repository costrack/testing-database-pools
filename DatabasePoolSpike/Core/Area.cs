﻿using System;

using NPoco;

namespace DatabasePoolSpike.Core
{
	[PrimaryKey("Id", AutoIncrement = false)]
	public class Area
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}