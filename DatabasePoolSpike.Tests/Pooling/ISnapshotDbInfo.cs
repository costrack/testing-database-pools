﻿using System;
using System.Data.SqlClient;

namespace DatabasePoolSpike.Pooling
{
	public interface ISnapshotDbInfo
		: IDbInfo
	{
		string SnapshotName { get; }
		SqlConnectionStringBuilder SnapshotCsb { get; }
		NPoco.Database SnapshotDb { get; }
	}
}