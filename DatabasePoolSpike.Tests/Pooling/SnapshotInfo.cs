﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Data;

using NPoco;

namespace DatabasePoolSpike.Pooling
{
	public class SnapshotInfo
		: ISnapshotDbInfo
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(SnapshotInfo));

		public SnapshotInfo(PoolInfo pool, string snapshotName)
		{
			Pool = pool;
			SnapshotName = snapshotName;
			Log.DebugFormat("Created: {0}", this);
		}

		public PoolInfo Pool { get; }
		public string SnapshotName { get; }
		public IDatabaseCreator Creator => Pool.Creator;
		public int PoolNumber => Pool.PoolNumber;
		public string MainDbName => Pool.MainDbName;
		public string PoolDbName => Pool.PoolDbName;
		public SqlConnectionStringBuilder PoolCsb => Pool.PoolCsb;

		public NPoco.Database Db => Pool.Db;

		public bool IsReadOnly => false;

		private SqlConnectionStringBuilder _snapshotCsb;
		public SqlConnectionStringBuilder SnapshotCsb
		{
			get
			{
				if (_snapshotCsb == null)
				{
					_snapshotCsb = PoolCsb.Clone();
					_snapshotCsb.InitialCatalog = SnapshotName;
				}
				return _snapshotCsb;
			}
		}

		private NPoco.Database _snapshotDb;
		public NPoco.Database SnapshotDb
		{
			get
			{
				if (_snapshotDb == null)
				{
					Log.DebugFormat("Opening NPoco database to {0}", SnapshotCsb);
					_snapshotDb = new LoggedDatabase(SnapshotCsb.ConnectionString, DatabaseType.SqlServer2012);
				}
				return _snapshotDb;
			}
		}

		private bool _disposed;

		public void Dispose()
		{
			Log.DebugFormat("Disposing {0}", this);
			_snapshotDb?.Connection?.ChangeDatabase("master");
			_snapshotDb?.Dispose();

			if (!_disposed)
			{
				_disposed = true;
				Snapshots.Revert(this);
			}

			Pool.Dispose();
		}

		public override string ToString()
		{
			return $"Snapshot {SnapshotName}, {Pool}";
		}
	}
}