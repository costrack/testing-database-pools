﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

using DatabasePoolSpike.Data;

using Shouldly;

namespace DatabasePoolSpike.Pooling
{
	public class Pools
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Pools));

		public static readonly SqlConnectionStringBuilder SessionsCsb = new SqlConnectionStringBuilder()
		{
			DataSource = @".\sqlexpress",
			InitialCatalog = "TestSessions",
			IntegratedSecurity = true,
		};

		private static readonly Dictionary<string, DateTime> MainDbCreatedDates = new Dictionary<string, DateTime>();

		public static PoolInfo AllocatePool(SqlConnectionStringBuilder csb, IDatabaseCreator creator)
		{
			Log.DebugFormat("Allocating pool for {0}, creator {1}", csb, creator);
			VerifySessionsDatabase();

			using (var cnn = new SqlConnection(SessionsCsb.ConnectionString))
			{
				cnn.Open();

				using (var tran = cnn.BeginTransaction())
				{
					// Lock the Sessions table
					Log.Debug("Locked the Sessions table");
					cnn.ExecuteNonQuery("SELECT 1 FROM Sessions WITH (TABLOCKX, HOLDLOCK)", tran);

					Cleanup(cnn, tran);
					Snapshots.Cleanup(csb, GetCurrentSessions(cnn, tran));

					string mainDbName = creator.BaseDbName;
					if (!string.IsNullOrEmpty(creator.Signature))
					{
						mainDbName += "_" + creator.Signature;
					}

					VerifyMainDbExists(csb, creator, mainDbName);

					// Find an available pool
					using (var command = cnn.CreateCommand())
					{
						command.Transaction = tran;
						command.CommandText = "select top 1 * from Sessions where DB = @db and Finished is not null order by PoolNumber";
						command.Parameters.AddWithValue("db", mainDbName);

						int? poolNumber = null;
						using (var dr = command.Logged().ExecuteReader())
						{
							if (dr.Read())
							{
								poolNumber = dr.GetInt32(1);
								Log.DebugFormat("Found available pool #{0}", poolNumber);
							}
						}

						if (poolNumber == null)
						{
							command.CommandText = "select max(PoolNumber) from Sessions where DB = @db";

							object result = command.Logged().ExecuteScalar();
							if (!(result is DBNull))
							{
								poolNumber = (int)result + 1;
								Log.DebugFormat("Creating new pool #{0}", poolNumber);
							}
							else
							{
								Log.Debug("No existing pools found");
								poolNumber = 0;
							}
						}

						command.Parameters.AddWithValue("poolNumber", poolNumber.Value);
						command.CommandText = "delete from Sessions where DB = @db and PoolNumber = @poolNumber";
						command.Logged().ExecuteNonQuery();

						command.CommandText = "insert into Sessions (DB, PoolNumber, PID, ConnectionString) values (@db, @poolNumber, @PID, @connectionString)";
						command.Parameters.AddWithValue("PID", System.Diagnostics.Process.GetCurrentProcess().Id);
						command.Parameters.AddWithValue("connectionString", csb.ConnectionString);
						command.Logged().ExecuteNonQuery();

						string poolDbName = GetPoolDatabaseName(mainDbName, poolNumber.Value);

						BackupHelper.RestoreIfMissing(csb, mainDbName, poolDbName);

						tran.Commit();
						Log.Debug("Releases Sessions table lock");

						Log.InfoFormat("Using database '{0}', main DB '{1}', pool #{2}", poolDbName, mainDbName, poolNumber);

						var poolCsb = csb.Clone();
						poolCsb.InitialCatalog = poolDbName;

						cnn.ChangeDatabase("master"); // Make certain no databases are being locked
						return new PoolInfo(creator, poolNumber.Value, mainDbName, poolDbName, poolCsb);
					}
				}
			}
		}

		public static string GetPoolDatabaseName(string mainDbName, int poolNumber)
		{
			var poolDbName = mainDbName;
			if (poolNumber > 0)
			{
				poolDbName += "_" + poolNumber.ToString().PadLeft(3, '0');
			}
			return poolDbName;
		}

		private static List<TestSession> GetCurrentSessions(SqlConnection cnn, SqlTransaction tran)
		{
			using (var command = cnn.CreateCommand())
			{
				command.Transaction = tran;
				command.CommandText = "select * from Sessions";

				using (var dr = command.Logged().ExecuteReader())
				{
					var ordinals = new
					{
						DB = dr.GetOrdinal("DB"),
						PoolNumber = dr.GetOrdinal("PoolNumber"),
						PID = dr.GetOrdinal("PID"),
						Started = dr.GetOrdinal("Started"),
						Finished = dr.GetOrdinal("Finished"),
						ConnectionString = dr.GetOrdinal("ConnectionString"),
					};
					var result = new List<TestSession>();
					while (dr.Read())
					{
						var session = new TestSession()
						{
							DB = dr.GetString(ordinals.DB),
							PoolNumber = dr.GetInt32(ordinals.PoolNumber),
							PID = dr.GetInt32(ordinals.PID),
							Started = dr.GetDateTime(ordinals.Started),
							Finished = dr.IsDBNull(ordinals.Finished) ? (DateTime?)null : dr.GetDateTime(ordinals.Finished),
							ConnectionString = dr.GetString(ordinals.ConnectionString),
						};
						result.Add(session);
					}
					return result;
				}
			}
		}

		private static void Cleanup(SqlConnection cnn, SqlTransaction tran)
		{
			var delete = new List<TestSession>();
			foreach (var session in GetCurrentSessions(cnn, tran))
			{
				if (session.Finished != null && (DateTime.Now - session.Finished.Value).TotalHours >= 24)
				{
					delete.Add(session);
					continue;
				}

				if (!cnn.DatabaseExists(session.PoolDbName, tran))
				{
					delete.Add(session);
					continue;
				}

				var age = DateTime.Now - session.Started;
				if (age.TotalHours > 1 && !IsValidPid(session.PID))
				{
					delete.Add(session);
				}
			}

			if (delete.Count == 0)
			{
				return;
			}

			using (var command = cnn.CreateCommand())
			{
				command.Transaction = tran;
				command.CommandText = "delete from Sessions where DB = @db and PoolNumber = @poolNumber";

				var parameters = new
				{
					Db = command.Parameters.Add("db", SqlDbType.NVarChar, 255),
					PoolNumber = command.Parameters.Add("poolNumber", SqlDbType.Int),
				};

				foreach (var session in delete)
				{
					parameters.Db.Value = session.DB;
					parameters.PoolNumber.Value = session.PoolNumber;

					Log.InfoFormat("Deleting stale session for DB '{0}', pool #{1}", session.DB, session.PoolNumber);

					command.Logged().ExecuteNonQuery();

					if (session.PoolDbName != session.DB) // Don't delete the seed database
					{
						using (var deleteDbCnn = new SqlConnection(session.ConnectionString))
						{
							deleteDbCnn.Open();

							if (deleteDbCnn.DatabaseExists(session.PoolDbName))
							{
								Log.DebugFormat("Deleting stale database {0}", session.PoolDbName);
								try
								{
									deleteDbCnn.ExecuteNonQuery("drop database [" + session.PoolDbName + "]");
								}
								catch (SqlException ex)
								{
									if (ex.Message.Contains("while the database snapshot"))
									{
										// Delete the snapshot and try cleaning up again
										var snapshotName = Regex.Match(ex.Message, "while the database snapshot \"(?<snapshotName>[^\"]*)\"").Groups["snapshotName"].Value;
										Log.DebugFormat("Drop failed. Dropping snapshot {0} on {1} and trying again", snapshotName, session.PoolDbName);
										deleteDbCnn.ExecuteNonQuery("drop database [" + snapshotName + "]");
										Cleanup(cnn, tran);
										return;
									}
									throw;
								}
							}
						}
					}
				}
			}
		}

		private static bool IsValidPid(int pid)
		{
			var processes = System.Diagnostics.Process.GetProcesses()
				.Select(p => p.Id)
				.ToHashSet();
			return processes.Contains(pid);
		}

		private static void VerifyMainDbExists(SqlConnectionStringBuilder csb, IDatabaseCreator dbCreator, string mainDbName)
		{
			DateTime mainDbCreated;
			if (!MainDbCreatedDates.TryGetValue(mainDbName, out mainDbCreated))
			{
				// If database exists, make it read-write
				using (var cnn = new SqlConnection(csb.ConnectionString))
				{
					cnn.Open();
					cnn.ChangeDatabase("master");

					if (cnn.DatabaseExists(mainDbName))
					{
						Log.DebugFormat("Enabling read/write on database {0}", mainDbName);
						cnn.ExecuteNonQuery("alter database [" + mainDbName + "] set read_write with no_wait");

						SqlConnectionExtensions.SysDatabaseInformation dbInfo;
						cnn.DatabaseExists(mainDbName, null, out dbInfo);
						dbInfo.IsReadOnly.ShouldBe(false);
					}
				}

				Log.DebugFormat("Migrating database {0}", mainDbName);
				var info = dbCreator.CreateOrMigrateDatabase(csb, mainDbName);
				Snapshots.AddSnapshotProcedure(csb, mainDbName);
				mainDbCreated = info.CreateDate;
				MainDbCreatedDates.Add(mainDbName, mainDbCreated);

				// Flip back to read-only
				using (var cnn = new SqlConnection(csb.ConnectionString))
				{
					cnn.Open();
					cnn.ChangeDatabase("master");

					Log.DebugFormat("Disabling read/write on database {0}", mainDbName);
					cnn.ExecuteNonQuery("alter database [" + mainDbName + "] set read_only with no_wait");

					SqlConnectionExtensions.SysDatabaseInformation dbInfo;
					cnn.DatabaseExists(mainDbName, null, out dbInfo);
					dbInfo.IsReadOnly.ShouldBe(true);
				}
			}
			BackupHelper.VerifyBackupExists(csb, mainDbName, mainDbCreated);
		}

		private static readonly HashSet<Guid> DeallocatedPools = new HashSet<Guid>();

		public static void DeallocatePool(PoolInfo poolInfo)
		{
			lock (DeallocatedPools)
			{
				if (DeallocatedPools.Contains(poolInfo.Id))
				{
					return;
				}

				DeallocatedPools.Add(poolInfo.Id);

				Log.InfoFormat("Finishing database session on DB '{0}', main DB '{1}', pool #{2}", poolInfo.PoolNumber, poolInfo.MainDbName, poolInfo.PoolNumber);
				using (var cnn = new SqlConnection(SessionsCsb.ConnectionString))
				{
					cnn.Open();

					using (var tran = cnn.BeginTransaction())
					{
						// Lock the Sessions table
						Log.Debug("Locked the Sessions table");
						cnn.ExecuteNonQuery("SELECT 1 FROM Sessions WITH (TABLOCKX, HOLDLOCK)", tran);

						using (var command = cnn.CreateCommand())
						{
							command.Transaction = tran;
							command.CommandText = "update Sessions set Finished = GETDATE() where DB = @db and PoolNumber = @poolNumber";
							command.Parameters.AddWithValue("db", poolInfo.MainDbName);
							command.Parameters.AddWithValue("poolNumber", poolInfo.PoolNumber);

							command.Logged().ExecuteNonQuery();
						}

						tran.Commit();
						Log.Debug("Releases Sessions table lock");
					}
				}
			}
		}

		private static readonly object VerifiedLock = new object();
		private static bool _verified;

		private static void VerifySessionsDatabase()
		{
			lock (VerifiedLock)
			{
				if (_verified)
				{
					return;
				}

				var csb = SessionsCsb.Clone();
				var dbName = csb.InitialCatalog;
				csb.InitialCatalog = "master";

				using (var cnn = new SqlConnection(csb.ConnectionString))
				{
					cnn.Open();
					if (cnn.DatabaseExists(dbName))
					{
						return;
					}

					Log.InfoFormat("Creating {0} database for pool tracking", dbName);
					using (var command = cnn.CreateCommand())
					{
						command.CommandText = $"create database [{dbName}]";
						command.Logged().ExecuteNonQuery();
					}

					cnn.ChangeDatabase(dbName);

					using (var command = cnn.CreateCommand())
					{
						command.CommandText = @"
create table Sessions (
	DB nvarchar(255) not null
	, PoolNumber int not null
	, PID int not null
	, Started datetime not null default GETDATE()
	, Finished datetime null
	, ConnectionString nvarchar(max) not null
	, PRIMARY KEY (DB, PoolNumber)
)";
						command.Logged().ExecuteNonQuery();
					}

					cnn.ChangeDatabase("master"); // Make certain no databases are being locked
				}

				_verified = true;
			}
		}
	}
}