﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;

using DatabasePoolSpike.Data;

namespace DatabasePoolSpike.Pooling
{
	public static class BackupHelper
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(BackupHelper));

		public static readonly DirectoryInfo BackupDirectory = new DirectoryInfo(@"C:\Temp");

		private static FileInfo GetBackupFile(string mainDbName)
		{
			var backupFile = new FileInfo(Path.Combine(BackupDirectory.FullName, mainDbName + ".bak"));
			return backupFile;
		}

		public static void VerifyBackupExists(SqlConnectionStringBuilder csb, string mainDbName, DateTime mainDbCreated)
		{
			var backupFile = GetBackupFile(mainDbName);
			if (backupFile.Exists && backupFile.CreationTime == mainDbCreated)
			{
				return;
			}

			if (backupFile.Exists)
			{
				Log.InfoFormat("Deleting stale backup at {0}", backupFile);
				backupFile.Delete();
			}

			Log.InfoFormat("Backing up database {0} to {1}", mainDbName, backupFile);
			using (var cnn = new SqlConnection(csb.ConnectionString))
			{
				cnn.Open();
				cnn.TraceInfoMessages(Log);

				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "backup database [" + mainDbName + "] to disk=@backupFile with stats = 10";
					command.Parameters.AddWithValue("backupFile", backupFile.FullName);

					command.Logged().ExecuteNonQuery();
				}

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
			}

			backupFile.CreationTime = mainDbCreated;
		}

		public static void RestoreIfMissing(SqlConnectionStringBuilder csb, string mainDbName, string poolDbName)
		{
			var backupFile = GetBackupFile(mainDbName);

			using (var cnn = new SqlConnection(csb.ConnectionString))
			{
				cnn.Open();
				if (cnn.DatabaseExists(poolDbName))
				{
					return;
				}

				Log.InfoFormat("Restoring pool database '{0}' from '{1}'", poolDbName, backupFile);

				var filesByLogicalName = new Dictionary<string, string>();
				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "restore filelistonly from disk = @backupFile";
					command.Parameters.AddWithValue("backupFile", backupFile.FullName);

					using (var dr = command.Logged().ExecuteReader())
					{
						var ordinals = new
						{
							LogicalName = dr.GetOrdinal("LogicalName"),
							PhysicalName = dr.GetOrdinal("PhysicalName"),
						};
						while (dr.Read())
						{
							var logicalName = dr.GetString(ordinals.LogicalName);
							var physicalName = dr.GetString(ordinals.PhysicalName);

							filesByLogicalName.Add(logicalName, physicalName);
						}
					}
				}

				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "restore database [" + poolDbName + "] from disk=@backupFile with file = 1";
					command.Parameters.AddWithValue("backupFile", backupFile.FullName);

					int fileIndex = -1;
					foreach (var pair in filesByLogicalName)
					{
						fileIndex++;
						var logicalName = pair.Key;
						var physicalName = pair.Value;

						command.CommandText += "\n, move @logicalName" + fileIndex + " to @physicalName" + fileIndex;

						command.Parameters.AddWithValue("logicalName" + fileIndex, logicalName);
						var newName = poolDbName + "_" + logicalName + Path.GetExtension(physicalName);
						var path = Path.Combine(Path.GetDirectoryName(physicalName), newName);
						command.Parameters.AddWithValue("physicalName" + fileIndex, path);
					}

					command.CommandText += ", stats = 10";

					cnn.TraceInfoMessages(Log);
					command.Logged().ExecuteNonQuery();
				}

				cnn.ChangeDatabase("master");

				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "alter database [" + poolDbName + "] set read_only with no_wait";
					command.Logged().ExecuteNonQuery();
				}
			}
		}
	}
}