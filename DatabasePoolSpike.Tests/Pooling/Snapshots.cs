﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

using DatabasePoolSpike.Data;

namespace DatabasePoolSpike.Pooling
{
	public static class Snapshots
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(Snapshots));

		public static SnapshotInfo AllocateAndSnapshot(SqlConnectionStringBuilder csb, IDatabaseCreator creator)
		{
			csb = csb ?? Pools.SessionsCsb;

			var pool = Pools.AllocatePool(csb, creator);
			return Create(pool);
		}

		public static SnapshotInfo Create(PoolInfo pool)
		{
			Log.InfoFormat("Creating snapshot for {0}", pool);
			using (var cnn = new SqlConnection(pool.PoolCsb.ConnectionString))
			{
				cnn.Open();

				// Only allow taking one snapshot per database
				using (var command = cnn.CreateCommand())
				{
					command.CommandText = @"
select
	snap.name
from sys.databases snap
inner join sys.databases orig on snap.source_database_id = orig.database_id
where orig.name = @name";
					command.Parameters.AddWithValue("name", pool.PoolDbName);

					using (var dr = command.Logged().ExecuteReader())
					{
						if (dr.Read())
						{
							throw new InvalidOperationException($"Database {pool.PoolDbName} already has a snapshot named {dr.GetString(0)}");
						}
					}
				}

				string snapshotName;
				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "usp_createsnapshot";
					command.CommandType = CommandType.StoredProcedure;
					command.Parameters.AddWithValue("dbname", pool.PoolDbName);
					command.Parameters.Add("snapshotname", SqlDbType.NVarChar, 255).Direction = ParameterDirection.Output;

					command.Logged().ExecuteNonQuery();

					snapshotName = (string)command.Parameters["snapshotname"].Value;
					Log.InfoFormat("Created snapshot '{0}' from DB '{1}'", snapshotName, pool.PoolDbName);
				}

				using (var command = cnn.CreateCommand())
				{
					Log.InfoFormat("Enabling read/write on database {0}", pool.PoolDbName);
					command.CommandText = "alter database [" + pool.PoolDbName + "] set read_write with no_wait";
					command.Logged().ExecuteNonQuery();
				}

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
				return new SnapshotInfo(pool, snapshotName);
			}
		}

		internal static void AddSnapshotProcedure(SqlConnectionStringBuilder csb, string database)
		{
			using (var cnn = new SqlConnection(csb.ConnectionString))
			{
				cnn.Open();
				cnn.ChangeDatabase(database);

				using (var command = cnn.CreateCommand())
				{
					command.CommandText = "select count(*) from sys.procedures where name = 'usp_createsnapshot'";
					int found = Convert.ToInt32(command.Logged().ExecuteScalar());
					if (found == 0)
					{
						Log.InfoFormat("Adding usp_createsnapshot procedure to database {0}", database);
						var embeddedSnapshotProcedureName = typeof(Snapshots).Assembly.GetManifestResourceNames()
							.Single(x => x.EndsWith("usp_createsnapshot.sql"));
						using (var stream = typeof(Snapshots).Assembly.GetManifestResourceStream(embeddedSnapshotProcedureName))
						{
							System.Diagnostics.Debug.Assert(stream != null);
							using (var sr = new StreamReader(stream))
							{
								command.CommandText = sr.ReadToEnd();
							}
						}
						command.Logged().ExecuteNonQuery();
					}
				}

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
			}
		}

		public static void Revert(SnapshotInfo snapshot)
		{
			Log.InfoFormat("Reverting to snapshot: {0}", snapshot);
			using (var cnn = new SqlConnection(snapshot.PoolCsb.ConnectionString))
			{
				cnn.Open();
				cnn.ChangeDatabase("master");

				RevertSnapshot(cnn, null, snapshot.PoolDbName, snapshot.SnapshotName);

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
			}
		}

		private static void RevertSnapshot(SqlConnection cnn, SqlTransaction tran, string poolDbName, string snapshotName)
		{
			Log.InfoFormat("Reverting database '{0}' from snapshot '{1}'", poolDbName, snapshotName);
			using (var command = cnn.CreateCommand())
			{
				command.Transaction = tran;

				cnn.KillConnectionsTo(poolDbName, tran);
				cnn.KillConnectionsTo(snapshotName, tran);

				command.CommandText = "restore database [" + poolDbName + "] from database_snapshot = '" + snapshotName + "'";
				command.Logged().ExecuteNonQuery();

				command.CommandText = "drop database [" + snapshotName + "]";
				command.Logged().ExecuteNonQuery();

				command.CommandText = "alter database [" + poolDbName + "] set read_only with no_wait";
				command.Logged().ExecuteNonQuery();
			}
		}

		public static void Cleanup(SqlConnectionStringBuilder csb, List<TestSession> runningSessions)
		{
			Log.Debug("Snapshots.Cleanup()");
			csb = csb.Clone();
			csb.InitialCatalog = "master";

			var expectSnapshotsOnDatabases = runningSessions
				.Where(x => x.Finished == null)
				.Select(x => x.PoolDbName)
				.ToHashSet(StringComparer.OrdinalIgnoreCase);

			using (var cnn = new SqlConnection(csb.ConnectionString))
			{
				cnn.Open();

				var snapshots = new Dictionary<string, string>();
				using (var command = cnn.CreateCommand())
				{
					command.CommandText = @"
select
		snap.name
		, orig.name
	from sys.databases snap
	inner join sys.databases orig on snap.source_database_id = orig.database_id
	where snap.name like '%_pooling'
";
					using (var dr = command.Logged().ExecuteReader())
					{
						while (dr.Read())
						{
							string snapshotName = dr.GetString(0);
							string mainDbName = dr.GetString(1);
							snapshots.Add(snapshotName, mainDbName);
						}
					}
				}

				foreach (var pair in snapshots)
				{
					var snapshotName = pair.Key;
					var mainDbName = pair.Value;

					if (expectSnapshotsOnDatabases.Contains(mainDbName))
					{
						continue;
					}

					RevertSnapshot(cnn, null, mainDbName, snapshotName);
				}
			}
		}
	}
}