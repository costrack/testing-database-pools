﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Data;

using NPoco;

namespace DatabasePoolSpike.Pooling
{
	public class PoolInfo
		: IDbInfo
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(PoolInfo));

		public PoolInfo(IDatabaseCreator creator, int poolNumber, string mainDbName, string poolDbName, SqlConnectionStringBuilder poolCsb)
		{
			Creator = creator;
			PoolNumber = poolNumber;
			MainDbName = mainDbName;
			PoolDbName = poolDbName;
			PoolCsb = poolCsb;

			Log.DebugFormat("Created: {0}", this);
		}

		public Guid Id { get; } = Guid.NewGuid();

		public IDatabaseCreator Creator { get; }
		public int PoolNumber { get; }
		public string MainDbName { get; }
		public string PoolDbName { get; }
		public SqlConnectionStringBuilder PoolCsb { get; }

		private NPoco.Database _db;
		public NPoco.Database Db => _db ?? (_db = new LoggedDatabase(PoolCsb.ConnectionString, DatabaseType.SqlServer2012));

		public bool IsReadOnly => true;

		private bool _disposed;

		public void Dispose()
		{
			_db?.Connection?.ChangeDatabase("master");
			_db?.Dispose();

			if (!_disposed)
			{
				_disposed = true;
				Pools.DeallocatePool(this);
			}
		}

		public override string ToString()
		{
			return $"Main DB: {MainDbName}, Pool #{PoolNumber}, Connection: {PoolCsb.ConnectionString}";
		}
	}
}