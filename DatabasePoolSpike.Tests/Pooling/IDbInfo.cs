﻿using System;
using System.Data.SqlClient;

namespace DatabasePoolSpike.Pooling
{
	public interface IDbInfo
		: IDisposable
	{
		IDatabaseCreator Creator { get; }
		int PoolNumber { get; }
		string MainDbName { get; }
		string PoolDbName { get; }
		SqlConnectionStringBuilder PoolCsb { get; }
		NPoco.Database Db { get; }

		bool IsReadOnly { get; }
	}
}