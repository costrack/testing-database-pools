﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DatabasePoolSpike.Pooling
{
	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public class TestSession
	{
		public string DB { get; set; }
		public int PoolNumber { get; set; }
		public int PID { get; set; }
		public string ConnectionString { get; set; }
		public DateTime Started { get; set; }
		public DateTime? Finished { get; set; }

		public string PoolDbName => Pools.GetPoolDatabaseName(DB, PoolNumber);
	}
}