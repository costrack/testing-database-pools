﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Data;

namespace DatabasePoolSpike.Pooling
{
	public interface IDatabaseCreator
	{
		string BaseDbName { get; }
		string Signature { get; }

		SqlConnectionExtensions.SysDatabaseInformation CreateOrMigrateDatabase(SqlConnectionStringBuilder csb, string dbName);
	}
}