﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Core;
using DatabasePoolSpike.Data;
using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using NPoco;

using NUnit.Framework;

using Shouldly;

namespace DatabasePoolSpike
{
	[TestFixture]
	[Parallelizable(ParallelScope.All)]
	public class IndependentTest
	{
		[Test]
		public void run_snapshot()
		{
			SqlConnectionStringBuilder csb;
			using (var snapshot = Snapshots.AllocateAndSnapshot(Pools.SessionsCsb, new SpikeDatabaseCreator()))
			{
				csb = snapshot.PoolCsb;
				snapshot.Db.Insert(
					new Area()
					{
						Id = Guid.NewGuid(),
						Name = "Test",
					});
				snapshot.Db.Fetch<Area>().Count.ShouldBe(1);
			}

			using (var db = new LoggedDatabase(csb.ConnectionString, DatabaseType.SqlServer2012))
			{
				db.Fetch<Area>().Count.ShouldBe(0);
			}
		}

		[Test]
		public void run_readonly()
		{
			using (var pool = Pools.AllocatePool(Pools.SessionsCsb, new SpikeDatabaseCreator()))
			using (pool.Db.GetTransaction())
			{
				var ex = Assert.Throws<SqlException>(
					() => pool.Db.Insert(
						new Area()
						{
							Id = Guid.NewGuid(),
							Name = "Test",
						}));
				ex.Message.ShouldContain("because the database is read-only");
			}
		}
	}
}