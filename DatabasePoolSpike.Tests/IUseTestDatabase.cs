﻿using System;

using DatabasePoolSpike.Pooling;

namespace DatabasePoolSpike
{
	public interface IUseTestDatabase
	{
		IDbInfo Database { get; set; }
	}
}