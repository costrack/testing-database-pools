﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Data;
using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using log4net;

using NPoco;

using NUnit.Framework;

using Shouldly;

namespace DatabasePoolSpike
{
	[TestFixture]
	[EnableLogging]
	[Category("Integration")]
	public class SteppedCreationTests
		: IUseTestDatabase
	{
		public class Step1
			: IDatabaseCreationStep
		{
			public string Identifier => "Step 1";

			public void ApplyOn(SqlConnection cnn, NPoco.Database db, ILog log)
			{
				using (var command = cnn.CreateCommand().WithTransactionSet())
				{
					command.CommandText = "create table Step1 (Id int identity primary key, Foo nvarchar(30))";
					command.Logged(log).ExecuteNonQuery();
				}
			}
		}

		public class Step2
			: IDatabaseCreationStep
		{
			public string Identifier => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer tortor neque, blandit sit amet egestas ullamcorper, mattis id nunc. Cras pretium, erat vitae pretium rhoncus, dolor ligula tincidunt tortor, vel porta arcu sem vel ante. Pellentesque molesti";

			public void ApplyOn(SqlConnection cnn, NPoco.Database db, ILog log)
			{
				using (var command = cnn.CreateCommand().WithTransactionSet())
				{
					command.CommandText = "insert into Step1 (Foo) values ('1'), ('2'), ('3')";
					command.Logged(log).ExecuteNonQuery();
				}
			}
		}

		public IDbInfo Database { get; set; }

		[Test]
		[UsesDatabase(typeof(Step1), typeof(Step2))]
		public void clean_db_should_have_3_records()
		{
			var foos = Database.Db.Fetch<Step1Entity>();
			foos.Count.ShouldBe(3);
		}

		[Test]
		[UsesDatabase(typeof(Step1), typeof(Step2))]
		public void cannot_add_record()
		{
			var ex = Assert.Throws<SqlException>(() =>
				Database.Db.Save(new Step1Entity()
				{
					Foo = "Read only DB",
				}));
			ex.Message.ShouldContain("because the database is read-only");
		}

		[Test]
		[UsesDatabaseReadWrite(typeof(Step1), typeof(Step2))]
		public void can_add_record()
		{
			Database.Db.Save(new Step1Entity()
			{
				Foo = "Read only DB",
			});
		}

		[TableName("Step1")]
		[PrimaryKey("Id", AutoIncrement = true)]
		public class Step1Entity
		{
			public int Id { get; set; }
			public string Foo { get; set; }
		}
	}
}