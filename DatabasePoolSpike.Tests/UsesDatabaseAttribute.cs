﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace DatabasePoolSpike
{
	public class UsesDatabaseAttribute
		: PropertyAttribute, ITestAction, IApplyToContext
	{
		public string MainConnectionString { get; set; } = "Server=.; Integrated Security=SSPI";

		protected virtual ParallelScope ParallelScope => ParallelScope.All;
		protected virtual bool AllowWrite => false;

		protected IDatabaseCreator DatabaseCreator { get; }

		public UsesDatabaseAttribute(params Type[] creatorTypes)
			: this("Test", creatorTypes)
		{
		}

		public UsesDatabaseAttribute(string baseDbName, params Type[] creatorTypes)
		{
			if (creatorTypes == null || creatorTypes.Length == 0)
			{
				throw new ArgumentNullException(nameof(creatorTypes));
			}
			if (creatorTypes.Length == 1 && typeof(IDatabaseCreator).IsAssignableFrom(creatorTypes[0]))
			{
				DatabaseCreator = (IDatabaseCreator)Activator.CreateInstance(creatorTypes[0]);
			}
			else
			{
				var creator = new SteppedDatabaseCreator(baseDbName);
				foreach (var type in creatorTypes)
				{
					if (!typeof(IDatabaseCreationStep).IsAssignableFrom(type))
					{
						throw new ArgumentOutOfRangeException(nameof(creatorTypes), $"Type {type} must implement {typeof(IDatabaseCreationStep)}");
					}
					var step = (IDatabaseCreationStep)Activator.CreateInstance(type);
					creator.Steps.Add(step);
				}
				DatabaseCreator = creator;
			}
			Properties.Add("ParallelScope", ParallelScope);
		}

		public void ApplyToContext(TestExecutionContext context)
		{
			if (context.ParallelScope == ParallelScope.Default)
			{
				context.ParallelScope = GetEffectiveScope(context) & ParallelScope.ContextMask;
			}
		}

		public override void ApplyToTest(Test test)
		{
			base.ApplyToTest(test);

			if (test is TestMethod method)
			{
				
			}
		}

		protected virtual ParallelScope GetEffectiveScope(TestExecutionContext context)
		{
			bool? testHasAllowWrite = context.CurrentTest?.Method?.GetCustomAttributes<AllowWriteAttribute>(true).Any();
			bool? fixtureHasAllowWrite = context.CurrentTest?.TypeInfo?.GetCustomAttributes<AllowWriteAttribute>(true).Any();

			if (fixtureHasAllowWrite.GetValueOrDefault()
				|| testHasAllowWrite.GetValueOrDefault())
			{
				return ParallelScope.None;
			}
			return ParallelScope;
		}

		private static readonly HashSet<string> IntegrationTestCategories = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		{
			"Bench",
			"Integration",
			"Policy",
			"System",
		};

		internal static void EnsureRunningIntegrationTest(ITest test)
		{
			ITest foundOn = null;
			var parents = test.Chain(x => x.Parent);
			foreach (var check in parents)
			{
				object category;
				if (!check.Properties.ContainsKey("Category") || (category = check.Properties.Get("Category")) == null)
				{
					continue;
				}

				if (IntegrationTestCategories.Contains(category.ToString()))
				{
					foundOn = check;
					break;
				}
			}

			if (foundOn == null)
			{
				throw new InvalidOperationException("Databases should only be used on integration tests. Test " + test.FullName + " must be flagged with one of: " + string.Join(", ", IntegrationTestCategories));
			}
		}

		public ActionTargets Targets => ActionTargets.Test;

		protected readonly string id = nameof(UsesDatabaseAttribute) + Guid.NewGuid();

		public virtual void BeforeTest(ITest test)
		{
			EnsureRunningIntegrationTest(test);

			var csb = new SqlConnectionStringBuilder(MainConnectionString);
			var allowWrite = AllowWrite || test.Fixture is IUseReadWriteDatabase;
			if (test.Method.GetCustomAttributes<AllowWriteAttribute>(true).Length > 0)
			{
				allowWrite = true;
			}

			IDbInfo db;
			if (allowWrite)
			{
				db = Snapshots.AllocateAndSnapshot(csb, DatabaseCreator);
			}
			else
			{
				db = Pools.AllocatePool(csb, DatabaseCreator);
			}
			test.Properties.Add(id, db);

			if (test.Fixture is IUseTestDatabase useDatabase)
			{
				useDatabase.Database = db;
			}
			if (test.Fixture is IUseReadWriteDatabase useReadWriteDatabase)
			{
				useReadWriteDatabase.Database = (ISnapshotDbInfo)db;
			}
		}

		public virtual void AfterTest(ITest test)
		{
			if (test.Properties.ContainsKey(id))
			{
				var db = (IDbInfo)test.Properties.Get(id);
				db.Dispose();
			}
		}
	}
}