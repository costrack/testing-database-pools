﻿using System;

using NUnit.Framework;
using NUnit.Framework.Interfaces;
using NUnit.Framework.Internal;

namespace DatabasePoolSpike
{
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
	public class AllowWriteAttribute
		: Attribute, IApplyToContext
	{
		public void ApplyToContext(TestExecutionContext context)
		{
			context.ParallelScope = ParallelScope.None;
		}
	}
}