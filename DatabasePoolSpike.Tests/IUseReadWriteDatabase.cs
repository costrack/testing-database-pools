﻿using System;

using DatabasePoolSpike.Pooling;

namespace DatabasePoolSpike
{
	public interface IUseReadWriteDatabase
	{
		ISnapshotDbInfo Database { get; set; }
	}
}