﻿using System;

using DatabasePoolSpike.Pooling;

using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace DatabasePoolSpike
{
	public class UsesDatabaseReadWriteAttribute
		: UsesDatabaseAttribute
	{
		public UsesDatabaseReadWriteAttribute(params Type[] creatorTypes)
			: base(creatorTypes)
		{
		}

		public UsesDatabaseReadWriteAttribute(string baseDbName, params Type[] creatorTypes)
			: base(baseDbName, creatorTypes)
		{
		}

		protected override ParallelScope ParallelScope => ParallelScope.None;

		protected override bool AllowWrite => true;

		public override void BeforeTest(ITest test)
		{
			base.BeforeTest(test);
			if (test.Fixture is IUseReadWriteDatabase useSnapshot)
			{
				var info = (ISnapshotDbInfo)test.Properties.Get(id);
				useSnapshot.Database = info;
			}
		}
	}
}