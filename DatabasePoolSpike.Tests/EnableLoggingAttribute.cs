﻿using System;

using DatabasePoolSpike.Data;

using log4net.Appender;
using log4net.Core;
using log4net.Layout;

using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace DatabasePoolSpike
{
	public class EnableLoggingAttribute
		: TestActionAttribute
	{
		private readonly bool _logSqlStatements;

		public EnableLoggingAttribute(bool logSqlStatements = false)
		{
			_logSqlStatements = logSqlStatements;
		}

		public static readonly Level Threshold = Level.Debug;

		public override ActionTargets Targets => ActionTargets.Suite;

		private static bool _loggingConfigured;

		public override void BeforeTest(ITest test)
		{
			base.BeforeTest(test);

			if (!_loggingConfigured)
			{
				_loggingConfigured = true;

				log4net.Config.BasicConfigurator.Configure(new NUnitAppender());
			}

			if (_logSqlStatements)
			{
				RuntimeLoggingHelper.SetLoggerLevel(typeof(CommandLogger), Level.Debug);
			}
			else
			{
				RuntimeLoggingHelper.SetLoggerLevel(typeof(CommandLogger), Level.Info);
			}
		}

		public class NUnitAppender
			: TraceAppender
		{
			public NUnitAppender()
			{
				Layout = new PatternLayout("%date %-5level %logger - %message%newline");
				Threshold = EnableLoggingAttribute.Threshold;
			}

			protected override void Append(LoggingEvent loggingEvent)
			{
				var rendered = RenderLoggingEvent(loggingEvent);
				if (loggingEvent.Level >= Level.Error)
				{
					TestContext.Error.Write(rendered);
				}
				else
				{
					TestContext.Out.Write(rendered);
				}
			}
		}
	}
}