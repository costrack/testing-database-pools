﻿using System;
using System.Data.SqlClient;

using log4net;

namespace DatabasePoolSpike.DatabaseCreators
{
	public interface IDatabaseCreationStep
	{
		string Identifier { get; }

		void ApplyOn(SqlConnection cnn, NPoco.Database db, ILog log);
	}
}