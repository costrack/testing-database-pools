﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Data;
using DatabasePoolSpike.Database;
using DatabasePoolSpike.Pooling;

using Shouldly;

namespace DatabasePoolSpike.DatabaseCreators
{
	public class SpikeDatabaseCreator
		: IDatabaseCreator
	{
		public static readonly SqlConnectionStringBuilder SpikeCsb = new SqlConnectionStringBuilder()
		{
			DataSource = @".\sqlexpress",
			InitialCatalog = "master",
			IntegratedSecurity = true,
		};

		public string BaseDbName => "Foo";

		public string Signature => "Signature";

		public SqlConnectionExtensions.SysDatabaseInformation CreateOrMigrateDatabase(SqlConnectionStringBuilder csb, string dbName)
		{
			using (var cnn = SqlConnectionExtensions.OpenConnectionToCreatedDb(csb, dbName, out var info))
			{
				var migrator = new Migrator();
				migrator.Migrate(cnn);

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
				return info;
			}
		}
	}
}