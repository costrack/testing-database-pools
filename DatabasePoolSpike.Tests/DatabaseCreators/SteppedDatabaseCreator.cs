﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using DatabasePoolSpike.Data;
using DatabasePoolSpike.Pooling;

using NPoco;

namespace DatabasePoolSpike.DatabaseCreators
{
	public class SteppedDatabaseCreator
		: IDatabaseCreator
	{
		public SteppedDatabaseCreator(string baseDbName = "Test")
		{
			BaseDbName = baseDbName;
		}

		public List<IDatabaseCreationStep> Steps { get; } = new List<IDatabaseCreationStep>();

		public string BaseDbName { get; }

		private int MaximumSignatureLength => 128 - BaseDbName.Length;

		private string _signature;
		public string Signature
		{
			get
			{
				if (_signature != null)
				{
					return _signature;
				}

				var identifiers = Steps.Select(x => x.Identifier).ToArray();

				string joined = string.Join("_", identifiers);
				if (joined.Length <= MaximumSignatureLength)
				{
					return _signature = joined;
				}

				var onlyCapitals = identifiers.Select(x => new string(x.Where(c => char.IsUpper(c) || char.IsDigit(c)).ToArray())).ToArray();
				joined = string.Join("_", onlyCapitals);
				if (joined.Length <= MaximumSignatureLength)
				{
					return _signature = joined;
				}

				// Without the "_" space
				joined = string.Join("", onlyCapitals);
				if (joined.Length <= MaximumSignatureLength)
				{
					return _signature = joined;
				}

				// Hash the full identifiers and use that
				var sb = new StringBuilder();
				using (var algorithm = MD5.Create())
				{
					var bytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(string.Join(" ", identifiers)));
					foreach (var b in bytes)
					{
						sb.Append(b.ToString("X2"));
					}
				}
				return _signature = "SHA_" + sb;
			}
		}

		public SqlConnectionExtensions.SysDatabaseInformation CreateOrMigrateDatabase(SqlConnectionStringBuilder csb, string dbName)
		{
			using (var cnn = SqlConnectionExtensions.OpenConnectionToCreatedDb(csb, dbName, out var info))
			{
				var log = log4net.LogManager.GetLogger(GetType());

				int appliedStepsTableCount = Convert.ToInt32(cnn.ExecuteScalar("select count(*) from information_schema.tables where table_name = '_AppliedSteps'", log: log));
				if (appliedStepsTableCount == 0)
				{
					cnn.ExecuteNonQuery("create table _AppliedSteps (Identifier nvarchar(255) primary key, AppliedOn datetime not null default getdate())", log: log);
				}

				using (var db = new LoggedDatabase(cnn, DatabaseType.SqlServer2012))
				{
					db.Log = log;

					var appliedSteps = new HashSet<string>(db.Fetch<AppliedStep>().Select(x => x.Identifier), StringComparer.OrdinalIgnoreCase);

					foreach (var step in Steps)
					{
						if (appliedSteps.Contains(step.Identifier))
						{
							continue;
						}

						using (var tran = db.GetTransaction())
						{
							var stepLog = log4net.LogManager.GetLogger($"{log.Logger.Name}.{step.Identifier}");
							step.ApplyOn(cnn, db, stepLog);

							db.Save(new AppliedStep()
							{
								Identifier = step.Identifier,
							});
							appliedSteps.Add(step.Identifier);
							tran.Complete();
						}
					}
				}

				cnn.ChangeDatabase("master"); // Make certain no databases are being locked
				return info;
			}
		}

		[TableName("_AppliedSteps")]
		[PrimaryKey("Identifier", AutoIncrement = false)]
		public class AppliedStep
		{
			public string Identifier { get; set; }
		}
	}
}