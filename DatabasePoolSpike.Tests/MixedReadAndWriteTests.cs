﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

using DatabasePoolSpike.Core;
using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using NUnit.Framework;

using Shouldly;

namespace DatabasePoolSpike
{
	[TestFixture]
	[EnableLogging(true)]
	[Category("Integration")]
	public class MixedReadAndWriteTests
		: IUseTestDatabase
	{
		private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(MixedReadAndWriteTests));

		[Test]
		[UsesDatabase(typeof(SpikeDatabaseCreator))]
		[Category("Foo")]
		public void run()
		{
			Log.Info("Running test");
		}

		[Test]
		[UsesDatabase(typeof(SpikeDatabaseCreator))]
		public void error_on_change()
		{
			var ex = Assert.Throws<SqlException>(
				() => Database.Db.Insert(
					new Area()
					{
						Id = Guid.NewGuid(),
						Name = "Test",
					}));
			ex.Message.ShouldContain("because the database is read-only");
		}

		private static int _runningCount;

		[Test]
		[UsesDatabase(typeof(SpikeDatabaseCreator))]
		[Combinatorial]
		public async Task can_run_multiple_concurrent([Values(1, 2, 3, 4, 5)] int value1, [Values(1, 2, 3, 4, 5)] int value2)
		{
			int running = Interlocked.Increment(ref _runningCount);
			if (running <= 1)
			{
				await Task.Delay(100);
				Interlocked.Decrement(ref _runningCount);
				Assert.Pass("Only me :(");
			}
			Database.Db.Query<Area>().ToList();
			Interlocked.Decrement(ref _runningCount);
		}

		[Test]
		[UsesDatabaseReadWrite(typeof(SpikeDatabaseCreator))]
		public void can_write_in_snapshot()
		{
			Log.Info("In snapshot");

			Database.Db.Insert(
				new Area()
				{
					Id = Guid.NewGuid(),
					Name = "Test",
				});
			Database.Db.Fetch<Area>().Count.ShouldBe(1);
		}

		public IDbInfo Database { get; set; }
	}
}