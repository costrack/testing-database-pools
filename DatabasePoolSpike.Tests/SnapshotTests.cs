﻿using System;
using System.Data.SqlClient;

using DatabasePoolSpike.Core;
using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using NUnit.Framework;

using Shouldly;

namespace DatabasePoolSpike
{
	[TestFixture]
	[EnableLogging]
	[UsesDatabaseReadWrite(typeof(SpikeDatabaseCreator))]
	[Category("Policy")]
	public class SnapshotTests
		: IUseReadWriteDatabase
	{
		[Test]
		public void changes_are_not_reflected_in_snapshot()
		{
			Database.Db.Insert(
				new Area()
				{
					Id = Guid.NewGuid(),
					Name = "Test",
				});
			Database.Db.Fetch<Area>().Count.ShouldBe(1);
			Database.SnapshotDb.Fetch<Area>().Count.ShouldBe(0);
		}

		[Test]
		public void snapshot_db_is_read_only()
		{
			var ex = Assert.Throws<SqlException>(
				() => Database.SnapshotDb.Insert(
					new Area()
					{
						Id = Guid.NewGuid(),
						Name = "Test",
					}));
			ex.Message.ShouldContain("because the database is read-only");
		}

		public ISnapshotDbInfo Database { get; set; }
	}
}