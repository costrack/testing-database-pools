﻿using System;

using DatabasePoolSpike.DatabaseCreators;
using DatabasePoolSpike.Pooling;

using NUnit.Framework;

using Shouldly;

namespace DatabasePoolSpike
{
	[TestFixture]
	[EnableLogging]
	[UsesDatabase(typeof(SpikeDatabaseCreator))]
	[Category("Integration")]
	public class FixtureTest
		: IUseTestDatabase
	{
		[Test]
		public void first()
		{
			Database.ShouldNotBe(null);
		}

		[Test]
		public void second()
		{
			Database.ShouldNotBe(null);
		}

		public IDbInfo Database { get; set; }
	}
}